from gym.envs.registration import register

register(
    id='t-env-v0',
    entry_point='trial_env.envs:TrialEnv',
)